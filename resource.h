//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by HTTPAPI.rc
//
#define IDD_HTTPAPI_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_IPADDRESS                   1000
#define IDC_CHECK_SSL                   1001
#define IDC_EDIT_USERNAME               1002
#define IDC_EDIT_PASSWORD               1003
#define IDC_EDIT_API                    1004
#define IDC_BUTTON_SEND                 1006
#define IDC_EDIT_RETURN                 1009
#define IDC_EDIT_PORT                   1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
