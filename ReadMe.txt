========================================================================
Code example: HTTPAPI
========================================================================

==Description==
This code example demonstrates how to send HTTP API using SDK to UDP 
device and receive return values.

==Requirements==
1. You need first configure your development environment, please refer
to http://url.cn/KDFC4g 	    
2. This code requires NVC/IPE series SDK or IPX/IPN series SDK. Please
download them first.
 
==How to use==
1. You should put this project in {SDK folder}\SRC\netclient\starttools\ 
directory.
2. This project originally provides 2 types of building configuration, 
Visual Studio 6 and Visual Studio 2010. You can make your own building 
configurations, such as VS2003/VS2008/VS2012, etc.