// HTTPAPIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HTTPAPI.h"
#include "HTTPAPIDlg.h"
#include "CHttpClient.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHTTPAPIDlg dialog

CHTTPAPIDlg::CHTTPAPIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHTTPAPIDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHTTPAPIDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CHTTPAPIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHTTPAPIDlg)
	DDX_Control(pDX, IDC_IPADDRESS, m_IPAddr);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CHTTPAPIDlg, CDialog)
	//{{AFX_MSG_MAP(CHTTPAPIDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHTTPAPIDlg message handlers

BOOL CHTTPAPIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	m_IPAddr.SetAddress(192, 168, 1, 28);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHTTPAPIDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHTTPAPIDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CHTTPAPIDlg::OnButtonSend() 
{
	// UDP device information, including IP, port, username, password.
	BYTE ipaddr[4] = {0,};
	m_IPAddr.GetAddress( ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3] );
	m_DeviceInfo.szWebServerIP.Format( "%d.%d.%d.%d", ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3] );

	m_DeviceInfo.iPort = 80;
	m_DeviceInfo.bSSL = FALSE;
	m_DeviceInfo.szUsername = "root";
	m_DeviceInfo.szPassword = "pass";

	//For API of "/nvc-cgi/admin/param.cgi?action=list&group=version", each component will be parsed as below:	
	char szCGI[MAX_HTTPAPI_LENGTH] = "/nvc-cgi/admin/param.cgi";
	char szOption[MAX_HTTPAPI_LENGTH] = "action=list&group=version";

	//Allocate buffer to store the returned value by HTTP API.
	DWORD dwresponse = 256 * 1024;
	BYTE* pcbresponse;
	pcbresponse = new BYTE[dwresponse];
	ZeroMemory( pcbresponse, dwresponse );

	//Send HTTP API to UDP device and receive the returned data
	_request_cgicommand (szCGI, szOption, pcbresponse, &dwresponse);

	((CEdit*)GetDlgItem(IDC_EDIT_RETURN))->SetWindowText((char*) pcbresponse);
	// Read all to linked list
}

BOOL CHTTPAPIDlg::_request_cgicommand (LPTSTR _lpszcgi, LPTSTR _lpszoption, BYTE* _lpcbresponse, DWORD* _pdwlength)
{
	CHttpClientBasic *pChttp = new CHttpClientBasic();
	pChttp->Init();
	
	HANDLE hSession = NULL;
	hSession = pChttp->Connect(m_DeviceInfo.szWebServerIP.GetBuffer(0), m_DeviceInfo.iPort, m_DeviceInfo.szUsername.GetBuffer(0), m_DeviceInfo.szPassword.GetBuffer(0));
	m_DeviceInfo.szWebServerIP.ReleaseBuffer();
	m_DeviceInfo.szUsername.ReleaseBuffer();
	m_DeviceInfo.szPassword.ReleaseBuffer();
	
	BOOL fRet = TRUE;
	HANDLE hRequest = pChttp->OpenRequest(hSession, m_DeviceInfo.bSSL, TRUE, _lpszcgi);
	fRet = pChttp->SendRequest(hRequest, _lpszoption, _lpcbresponse, _pdwlength);
	
	if (fRet == FALSE)
	{
		AfxMessageBox(_T("Faild to connect webserver!"));
	}
	
	pChttp->CloseRequest(hRequest);
	if (hSession)
		pChttp->Disconnect(hSession);
	if(pChttp != NULL)
	{
		pChttp->Deinit();
		delete pChttp;
	}
	return fRet;
}
