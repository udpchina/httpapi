// HTTPAPIDlg.h : header file
//

#if !defined(AFX_HTTPAPIDLG_H__253862E5_18B0_44CD_8753_8F1EF32118A2__INCLUDED_)
#define AFX_HTTPAPIDLG_H__253862E5_18B0_44CD_8753_8F1EF32118A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////////
#define MAX_HTTPAPI_LENGTH 128

/////////////////////////////////////////////////////////////////////////////
typedef struct tag_DeviceInfo
{	
	CString szWebServerIP;
	INT32 iPort;
	BOOL bSSL;
	CString szUsername;
	CString szPassword;
} DeviceInfo;


/////////////////////////////////////////////////////////////////////////////
// CHTTPAPIDlg dialog

class CHTTPAPIDlg : public CDialog
{
// Construction
public:
	CHTTPAPIDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CHTTPAPIDlg)
	enum { IDD = IDD_HTTPAPI_DIALOG };
	CIPAddressCtrl	m_IPAddr;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHTTPAPIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CHTTPAPIDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonSend();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL _request_cgicommand(LPTSTR _lpszcgi, LPTSTR _lpszoption, BYTE* _lpcbresponse, DWORD* _pdwlength );
	DeviceInfo m_DeviceInfo;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HTTPAPIDLG_H__253862E5_18B0_44CD_8753_8F1EF32118A2__INCLUDED_)
