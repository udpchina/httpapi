// HTTPAPI.h : main header file for the HTTPAPI application
//

#if !defined(AFX_HTTPAPI_H__AAF5B089_9216_49A0_9048_17C8BDB78215__INCLUDED_)
#define AFX_HTTPAPI_H__AAF5B089_9216_49A0_9048_17C8BDB78215__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CHTTPAPIApp:
// See HTTPAPI.cpp for the implementation of this class
//

class CHTTPAPIApp : public CWinApp
{
public:
	CHTTPAPIApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHTTPAPIApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CHTTPAPIApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HTTPAPI_H__AAF5B089_9216_49A0_9048_17C8BDB78215__INCLUDED_)
